const firebase =require('../db')
const Museum = require('../models/museum')
const firestore = firebase.firestore();

const addMuseum = async (req, res, next) => {
    try{
        const data = req.body
        await firestore.collection('museum').doc().set(data)
        res.send('Record saved successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getAllMuseums = async(req, res, next) => {
    try{
        const museum = await firestore.collection('museum')
        const data = await museum.get()
        const museumsArray = [];

        if(data.empty){
            res.status(400).send('No Museum Record Found')
        }else{
            data.forEach(doc => {
                const museum = new Museum(
                    doc.id,
                    doc.data().name_en,
                    doc.data().name_cn,
                    doc.data().address_en,
                    doc.data().address_cn,
                    doc.data().openingHours,
                    doc.data().telephone,
                    doc.data().website,
                    doc.data().pic_url,
                    doc.data().remark_en,
                    doc.data().remark_cn,
                    doc.data().floorPlan
                )
                museumsArray.push(museum)
            })

            res.send(museumsArray)
        }
        res.send('Record saved successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getMuseum = async(req, res, next) => {
    try{
        const id = req.params.id
        const museum = await firestore.collection('museum').doc(id)
        const data = await museum.get()

        if(!data.exists){
            res.status(400).send('Museum with the givenID not found')
        }else{
            res.send(data.data())
        }
        res.send('Record saved successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const updateMuseum = async (req, res, next) => {
    try{
        const id = req.params.id
        const data = req.body
        const museum = await firestore.collection('museum').doc(id)

        await museum.update(data)
        res.send('Museum record updated successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const deleteMuseum = async (req, res, next) => {
    try{
        const id = req.params.id
        await firestore.collection('museum').doc(id).delete()
        res.send('Record deleted successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

module.exports={
    addMuseum,
    getAllMuseums,
    getMuseum,
    updateMuseum,
    deleteMuseum
}