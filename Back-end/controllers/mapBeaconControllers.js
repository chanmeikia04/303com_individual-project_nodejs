const firebase =require('../db')
const AdditionBeacon = require('../models/mapBeacon')
const firestore = firebase.firestore();

const addMapBeacon = async (req, res, next) => {
    try{
        // res.send(req.body.UUID)
        const map = await firestore.collection('MapBeacon').where('UUID', "==", req.body.UUID).where('MAJOR', "==", req.body.MAJOR).where('MINOR', "==", req.body.MINOR).get()

        // console.log(map.data())
        if(map.empty){
            const data = req.body
            await firestore.collection('MapBeacon').doc().set(data)
            res.send('Record saved successfully')
        }else{
            res.status(400).send("This Blutooth Beacon is existed")
        }
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getAllMapBeacon = async(req, res, next) => {
    try{
        const mapInfoData = await firestore.collection('MapBeacon').get()
        const mapBeaconArray = []
        const resultArray = []

        if(mapInfoData.empty){
            res.status(400).send('No Museum Record Found')
        }else{
            mapInfoData.forEach(docOfMapBeacon => {
                mapBeaconArray.push(docOfMapBeacon)
            })

            
            
            for(var i =0; i<mapBeaconArray.length; i++){
            let museumArray = ""
            const museumData = await firestore.collection('museum').doc(mapBeaconArray[i].data().museumID).get()
            if(museumData.exists){
                museumArray = museumData.data()
                console.log(museumData)
            }else{
                console.log("null")
            }
            
                const areaData = await firestore.collection('MuseumArea').doc(mapBeaconArray[i].data().areaID).get()
                if(areaData.exists){
                    const mapBeacon = new AdditionBeacon(
                        mapBeaconArray[i].id,
                        mapBeaconArray[i].data().MAJOR,
                        mapBeaconArray[i].data().UUID,
                        mapBeaconArray[i].data().MINOR,
                        mapBeaconArray[i].data().areaID,
                        mapBeaconArray[i].data().museumID,
                        areaData.data().area_cn,
                        areaData.data().area_en,
                        museumArray.name_cn,
                        museumArray.name_en
                    )
                    resultArray.push(mapBeacon)
                }else{
                    console.log("AreaDate Not Exists")
                }
            }
            res.send(resultArray)
        }
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getMapBeaconByBeaconInfo = async(req, res, next) => {
    try{
        const uuid = req.params.uuid
        const minor = req.params.minor
        const major = req.params.major
        const mapBeaconArray = [];
        const resultArray = [];

        const mapInfoData = await firestore.collection('MapBeacon').where('UUID', '==', uuid).where('MAJOR', '==', major).where('MINOR', '==', minor).get()
        
        if (mapInfoData.empty) {
            res.status(400).send('Additional Information with the givenID not found')
            return;
        }

        mapInfoData.forEach(doc => {
            mapBeaconArray.push(doc)
        })

        for(var i =0; i<mapBeaconArray.length; i++){
            const areaData = await firestore.collection('MuseumArea').doc(mapBeaconArray[i].data().areaID).get()
            if(areaData.exists){
                const mapBeacon = new AdditionBeacon(
                    mapBeaconArray[i].id,
                    mapBeaconArray[i].data().MAJOR,
                    mapBeaconArray[i].data().UUID,
                    mapBeaconArray[i].data().MINOR,
                    mapBeaconArray[i].data().areaID,
                    mapBeaconArray[i].data().museumID,
                    areaData.data().area_cn,
                    areaData.data().area_en
                )
                resultArray.push(mapBeacon)
            }else{
                console.log("AreaData Not Exists")
            }
        }
        res.send(resultArray)
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getMapBeaconBeaconByID = async(req, res, next) => {
    try{
        const id = req.params.id
        const additionalInfo = await firestore.collection('MapBeacon').doc(id)
        const data = await additionalInfo.get()

        if(!data.exists){
            res.status(400).send('Additional Information with the givenID not found')
        }else{
            const areaData = await firestore.collection('MuseumArea').doc(data.data().areaID).get()
            if(areaData.exists){
                const mapBeacon = new AdditionBeacon(
                    data.data().id,
                    data.data().MAJOR,
                    data.data().UUID,
                    data.data().MINOR,
                    data.data().areaID,
                    data.data().museumID,
                    areaData.data().area_cn,
                    areaData.data().area_en
                )
                res.send(mapBeacon)
            }else{
                console.log("AreaData Not Exists")
            }
            
        }
    }catch(error){
        res.status(400).send(error.message)
    }
}

const updateMapBeacon = async (req, res, next) => {
    try{
        const id = req.params.id
        const data = req.body
        const additionalInfo = await firestore.collection('MapBeacon').doc(id)

        await additionalInfo.update(data)
        res.send('Museum record updated successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const deleteMapBeacon = async (req, res, next) => {
    try{
        const id = req.params.id

        const map = await firestore.collection('MapBeacon').doc(id).get()

        // console.log(map.data())
        if(map.exists){
            const additionalDB = await firestore.collection('AdditionalInformation').where('UUID', '==', map.data().UUID).where('MAJOR', '==', map.data().MAJOR).where('MINOR', '==', map.data().MINOR).get()
            console.log(additionalDB)
            if (additionalDB.empty) {
                await firestore.collection('MapBeacon').doc(id).delete()
                res.send('Record deleted successfully')
            }else{
                res.status(400).send('Remove the addtional information of this Bluetooth Beacon first.')
                return;
            }
        }

        
    }catch(error){
        res.status(400).send(error.message)
    }
}

module.exports={
    addMapBeacon, getAllMapBeacon, getMapBeaconByBeaconInfo, getMapBeaconBeaconByID, updateMapBeacon, deleteMapBeacon
}