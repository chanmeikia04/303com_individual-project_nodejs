const firebase =require('../db')
const Exhibition = require('../models/exhibition')
const firestore = firebase.firestore();

const addExhibition = async (req, res, next) => {
    try{
        const data = req.body
        await firestore.collection('exhibitions').doc().set(data)
        res.send('Record saved successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getAllExhibition = async(req, res, next) => {
    try{
        const exhibitionData = await firestore.collection('exhibitions').get()
        const exhibitionsArray = [];
        const resultArray = [];

        if(exhibitionData.empty){
            res.status(400).send('No Exhibition Record Found')
        }else{
            exhibitionData.forEach(doc => {
                exhibitionsArray.push(doc)
            })

            for(var i =0; i<exhibitionsArray.length; i++){
                console.log(exhibitionsArray[i].data().museumId)
                const museumData = await firestore.collection('museum').doc(exhibitionsArray[i].data().museumId).get()
                if(museumData.exists){
                    const exhibition = new Exhibition(
                        exhibitionsArray[i].id,
                        exhibitionsArray[i].data().museumId,
                        exhibitionsArray[i].data().exhibitionName_cn,
                        exhibitionsArray[i].data().exhibitionName_en,
                        exhibitionsArray[i].data().Date,
                        exhibitionsArray[i].data().Intro_cn,
                        exhibitionsArray[i].data().Intro_en,
                        exhibitionsArray[i].data().pic_url,
                        exhibitionsArray[i].data().website_url,
                        exhibitionsArray[i].data().isSpecialExhibition,
                        museumData.data().name_cn,
                        museumData.data().name_en
                    )
                    resultArray.push(exhibition)
                }else{
                    console.log("AreaDate Not Exists")
                }
            }

            res.send(resultArray)
        }
        res.send('Record saved successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getExhibitionByID = async(req, res, next) => {
    try{
        const id = req.params.id
        const exhibitionData = await firestore.collection('exhibitions').doc(id).get()

        if(!exhibitionData.exists){
            res.status(400).send('No Exhibition Record Found')
        }else{

                const museumData = await firestore.collection('museum').doc(exhibitionData.data().museumId).get()
                if(museumData.exists){
                    const exhibition = new Exhibition(
                        exhibitionData.id,
                        exhibitionData.data().museumId,
                        exhibitionData.data().exhibitionName_cn,
                        exhibitionData.data().exhibitionName_en,
                        exhibitionData.data().Date,
                        exhibitionData.data().Intro_cn,
                        exhibitionData.data().Intro_en,
                        exhibitionData.data().pic_url,
                        exhibitionData.data().website_url,
                        exhibitionData.data().isSpecialExhibition,
                        museumData.data().name_cn,
                        museumData.data().name_en
                    )
                    res.send(exhibition)
                }else{
                    console.log("AreaDate Not Exists")
                }
        

            
        }
        res.send('Record saved successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getExhibition = async(req, res, next) => {
    try{
        const id = req.params.id
        const exhibition = await firestore.collection('exhibitions').where('museumId', '==', id)
        const data = await exhibition.get()
        const exhibitionsArray = [];

        if (data.empty) {
            res.status(400).send('Exhibition with the givenID not found')
            return;
        }

        data.forEach(doc => {
            const exhibition = new Exhibition(
                doc.id,
                doc.data().museumId,
                doc.data().exhibitionName_cn,
                doc.data().exhibitionName_en,
                doc.data().Date,
                doc.data().Intro_cn,
                doc.data().Intro_en,
                doc.data().pic_url,
                doc.data().website_url,
                doc.data().isSpecialExhibition
            )
            exhibitionsArray.push(exhibition)
        })
          
        res.send(exhibitionsArray)
     
    }catch(error){
        res.status(400).send(error.message)
    }
}

const updateExhibition = async (req, res, next) => {
    try{
        const id = req.params.id
        const data = req.body
        const exhibition = await firestore.collection('exhibitions').doc(id)

        await exhibition.update(data)
        res.send('Exhibition record updated successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const deleteExhibition = async (req, res, next) => {
    try{
        const id = req.params.id
        await firestore.collection('exhibitions').doc(id).delete()
        res.send('Record deleted successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

module.exports={
    addExhibition,
    getAllExhibition,
    getExhibitionByID,
    getExhibition,
    updateExhibition,
    deleteExhibition
}