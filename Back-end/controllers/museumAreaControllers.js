const firebase =require('../db')
const MuseumArea = require('../models/museumArea')
const FloorPlan = require('../models/floorPlan');
const firestore = firebase.firestore();

const addArea = async (req, res, next) => {
    try{
        const data = req.body
        await firestore.collection('MuseumArea').doc().set(data)
        res.send('Record saved successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getAllArea = async(req, res, next) => {
    try{
        const museumAreaData = await firestore.collection('MuseumArea').get()
        const museumAreaArray = [];
        const resultArray = [];

        if(museumAreaData.empty){
            res.status(400).send('No Museum Area Record Found')
        }else{
            museumAreaData.forEach(doc => {
                museumAreaArray.push(doc)
            })
            
            for(var i =0; i<museumAreaArray.length; i++){
                const museumData = await firestore.collection('museum').doc(museumAreaArray[i].data().museumID).get()
                if(museumData.exists){
                    const museumArea = new MuseumArea(
                        museumAreaArray[i].id,
                        museumAreaArray[i].data().museumID,
                        museumAreaArray[i].data().area_cn,
                        museumAreaArray[i].data().area_en,
                        museumData.data().name_cn,
                        museumData.data().name_en,
                        museumAreaArray[i].data().pointX,
                        museumAreaArray[i].data().pointY,
                        museumData.data().floorPlan
                    )
                    resultArray.push(museumArea)
                }else{
                    console.log("AreaDate Not Exists")
                }
            }
            res.send(resultArray)
        }
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getAreaByID = async(req, res, next) => {
    try{
        const id = req.params.id
        const museumAreaData = await firestore.collection('MuseumArea').doc(id).get()

        if(!museumAreaData.exists){
            res.status(400).send('Museum Area with the givenID not found')
        }else{
            const museumData = await firestore.collection('museum').doc(museumAreaData.data().museumID).get()
            if(museumData.exists){
                const museumArea = new MuseumArea(
                    museumAreaData.id,
                    museumAreaData.data().museumID,
                    museumAreaData.data().area_cn,
                    museumAreaData.data().area_en,
                    museumData.data().name_cn,
                    museumData.data().name_en,
                    museumAreaData.data().pointX,
                    museumAreaData.data().pointY,
                    museumData.data().floorPlan
                )
                res.send(museumArea)
            }else{
                console.log("AreaDate Not Exists")
            }
        }
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getAreaByMuseumID = async(req, res, next) => {
    try{
        const areaList = []
        const id = req.params.id
        const museumAreaData = await firestore.collection("MuseumArea").where("museumID", "==", id).get()

        if (museumAreaData.empty) {
            res.status(400).send('Additional Information with the givenID not found')
            return;
        }

        museumAreaData.forEach(doc => {
            const museumArea = new MuseumArea(
                doc.id,
                doc.data().museumID,
                doc.data().area_cn,
                doc.data().area_en,
                doc.data().pointX,
                doc.data().pointY
            )
            areaList.push(museumArea)            
        })

        res.send(areaList)
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getAreaByTwoID = async(req, res, next) => {
    try{
        const id1 = req.params.id
        const id2 = req.params.id2
        const museumAreaData = await firestore.collection('MuseumArea').doc(id1).get()

        if(!museumAreaData.exists){
            res.status(400).send('Museum Area with the givenID not found')
        }else{
            const museumData = await firestore.collection('museum').doc(museumAreaData.data().museumID).get()
            if(museumData.exists){
                const museumAreaData1 = await firestore.collection('MuseumArea').doc(id2).get()

                if(!museumAreaData1.exists){
                    res.status(400).send('Museum Area(2) with the givenID not found')
                }else{
                    const museumData1 = await firestore.collection('museum').doc(museumAreaData1.data().museumID).get()
                    if(museumData1.exists){
                        const floorPlan = new FloorPlan(
                            museumAreaData.data().pointX,
                            museumAreaData.data().pointY,
                            museumAreaData1.data().pointX,
                            museumAreaData1.data().pointY,
                            museumData.data().floorPlan
                        )
                        res.send(floorPlan)
                    }else{
                        console.log("AreaDate Not Exists")
                    }
                }

                res.send(museumArea)
            }else{
                console.log("AreaDate Not Exists")
            }
        }

       


    }catch(error){
        res.status(400).send(error.message)
    }
}


const updateArea = async (req, res, next) => {
    try{
        const id = req.params.id
        const data = req.body
        const additionalInfo = await firestore.collection('MuseumArea').doc(id)

        await additionalInfo.update(data)
        res.send('Museum Area record updated successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const deleteArea = async (req, res, next) => {
    try{
        const id = req.params.id
        
        const MapBeaconDB = await firestore.collection('MapBeacon').where('areaID', '==', id).get()
        if (MapBeaconDB.empty) {
            await firestore.collection('MuseumArea').doc(id).delete()
            res.send('Record deleted successfully')
        }else{
            res.status(400).send('Modify all the areaID in Map Bluetooth Beacon first.')
            return;
        }
    }catch(error){
        res.status(400).send(error.message)
    }
}

module.exports={
    addArea, getAllArea, getAreaByID, getAreaByTwoID, updateArea, deleteArea, getAreaByMuseumID
}