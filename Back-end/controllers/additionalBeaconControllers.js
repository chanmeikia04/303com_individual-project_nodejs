const firebase =require('../db')
const AdditionBeacon = require('../models/additionBeacon')
const firestore = firebase.firestore();

const addAdditionBeacon = async (req, res, next) => {
    try{
        const data = req.body

        const mapInfoData = await firestore.collection('MapBeacon').where('UUID', '==', data.UUID).where('MAJOR', '==', data.MAJOR).where('MINOR', '==', data.MINOR).get()
        
        if (mapInfoData.empty) {
            res.status(400).send('Please create Map Beacon record first')
            return;
        }

        const additionalDB = await firestore.collection('AdditionalInformation').where('UUID', '==', data.UUID).where('MAJOR', '==', data.MAJOR).where('MINOR', '==', data.MINOR).get()
        if (additionalDB.empty) {
            await firestore.collection('AdditionalInformation').doc().set(data)
            res.send('Record saved successfully')
        }else{
            res.status(400).send('This Bluetooth Beacon is existed.')
            return;
        }
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getAllAdditionBeacon = async(req, res, next) => {
    try{
        const additionalInfo = await firestore.collection('AdditionalInformation')
        const data = await additionalInfo.get()
        const additionalBeaconArray = [];

        if(data.empty){
            res.status(400).send('No Museum Record Found')
        }else{
            data.forEach(doc => {
                const additionBeacon = new AdditionBeacon(
                    doc.id,
                    doc.data().pic_url,
                    doc.data().website_url,
                    doc.data().MAJOR,
                    doc.data().UUID,
                    doc.data().MINOR,
                    doc.data().name_cn,
                    doc.data().name_en
                )
                additionalBeaconArray.push(additionBeacon)
            })

            res.send(additionalBeaconArray)
        }
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getAdditionBeaconByBeaconInfo = async(req, res, next) => {
    try{
        const uuid = req.params.uuid
        const minor = req.params.minor
        const major = req.params.major
        const additionalBeaconArray = [];

        const additionalInfo = await firestore.collection('AdditionalInformation').where('UUID', '==', uuid).where('MAJOR', '==', major).where('MINOR', '==', minor)

        const data = await additionalInfo.get()
        

        if (data.empty) {
            res.status(400).send('Additional Information with the givenID not found')
            return;
        }

        data.forEach(doc => {
            const additional = new AdditionBeacon(
                doc.id,
                doc.data().pic_url,
                doc.data().website_url,
                doc.data().MAJOR,
                doc.data().UUID,
                doc.data().MINOR,
                doc.data().name_cn,
                doc.data().name_en
            )
            additionalBeaconArray.push(additional)
        })

        res.send(additionalBeaconArray)
    }catch(error){
        res.status(400).send(error.message)
    }
}

const getAdditionBeaconByID = async(req, res, next) => {
    try{
        const id = req.params.id
        const additionalInfo = await firestore.collection('AdditionalInformation').doc(id)
        const data = await additionalInfo.get()

        if(!data.exists){
            res.status(400).send('Additional Information with the givenID not found')
        }else{
            res.send(data.data())
        }
    }catch(error){
        res.status(400).send(error.message)
    }
}

const updateAdditionBeacon = async (req, res, next) => {
    try{
        const id = req.params.id
        const data = req.body
        const additionalInfo = await firestore.collection('AdditionalInformation').doc(id)

        await additionalInfo.update(data)
        res.send('Museum record updated successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

const deleteAdditionBeacon = async (req, res, next) => {
    try{
        const id = req.params.id
        await firestore.collection('AdditionalInformation').doc(id).delete()
        res.send('Record deleted successfully')
    }catch(error){
        res.status(400).send(error.message)
    }
}

module.exports={
    addAdditionBeacon,
    getAllAdditionBeacon,
    getAdditionBeaconByID,
    getAdditionBeaconByBeaconInfo,
    updateAdditionBeacon,
    deleteAdditionBeacon
}