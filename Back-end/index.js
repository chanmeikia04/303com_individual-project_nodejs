const admin = require('firebase-admin');
const serviceAccount = require('./ServiceAccountKey.json');

const express = require('express')
const app = express()

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();

//Import Routes
const museumRoute = require('./routes/museumPage');

app.use('/museum', museumRoute);


//Home Page
// app.get('/Museumdb', function (req, res) {
//     var result = getMuseumData();
//     result.then(function(value){
//         res.send(value)
//     })
// })

// app.get('/exhibition/:museumId', function (req, res) {
//     var result = getExhibitionData(req.params['museumId']);
//     result.then(function(value){
//         res.send(value)
//     })
// })

//Introduction Page
app.get('/BeaconsInfo/uuid/:uuid/major/:major/minor/:minor', function(req,res){
    var result = getBeaconsInfoData(req.params['uuid'],req.params['major'],req.params['minor']);
    result.then(function(value){
        res.send(value)
    })
})

//Map Page
app.get('/currPosition/uuid/:uuid/major/:major/minor/:minor', function(req,res){
    var result = getBeaconsMap(req.params['uuid'],req.params['major'],req.params['minor']);
    result.then(function(value){
        res.send(value)
    })
})


//Home Page
// async function getExhibitionData(museumId) {
//     const exhibitionsRef = db.collection('exhibitions');
//     const snapshot = await exhibitionsRef.where('museumId', '==', museumId).get();
//     if (snapshot.empty) {
//         console.log('No matching documents.');
//         return;
//     }

//     var result = '{"result":['
//     var cnt = 1
//     snapshot.forEach(doc => {
//         var tem = Object.assign({}, doc.data());
//         tem['exhibitionId'] = doc.id;

//         tem = JSON.stringify(tem);
        
//         if(cnt == 1){
//             result += tem
//             cnt++
//         }else{
//             result += ',' + tem
//             cnt++
//         }
//         console.log((tem))
//     });
//     result += ']}'
//     return JSON.parse(result)
// }

// async function getMuseumData() {
//     const museumRef = db.collection('museum');
//     const snapshot = await museumRef.get();
//     var result = '{"result":['
//     var cnt = 1

//     snapshot.forEach(doc => {
//         var tem = Object.assign({}, doc.data());
//         tem['museumId'] = doc.id;

//         tem = JSON.stringify(tem);
        
//         if(cnt == 1){
//             result += tem
//             cnt++
//         }else{
//             result += ',' + tem
//             cnt++
//         }
//         console.log((tem))
//     });

//     result += ']}'
//     return JSON.parse(result)
// }

//Introduction Page
async function getBeaconsInfoData(uuid, major, minor){
    const exhibitionsRef = db.collection('AdditionalInformation');
    const snapshot = await exhibitionsRef
    .where('UUID', '==', uuid)
    .where('MAJOR', '==', major)
    .where('MINOR', '==', minor)
    .get();

    if (snapshot.empty) {
        console.log('No matching documents.');
        return;
    }

    var result = '{"result":['
    var cnt = 1
    snapshot.forEach(doc => {
        var tem = Object.assign({}, doc.data());
        tem = JSON.stringify(tem);
        if(cnt == 1){
            result += tem
            cnt++
        }else{
            result += ',' + tem
            cnt++
        }
        // console.log((tem))
    });
    result += ']}'
    return JSON.parse(result)
}

//Map
async function getBeaconsMap(uuid, major, minor){
    //Get AreaID
    const MapBeaconsRef = db.collection('MapBeacon');
    const snapshot = await MapBeaconsRef
    .where('UUID', '==', uuid)
    .where('MAJOR', '==', major)
    .where('MINOR', '==', minor)
    .get();

    if (snapshot.empty) {
        console.log('No matching documents. - MapBeacon');
        return;
    }

    var areaID = null;
    var museumID = null; 
    var result = '{"result":['
    snapshot.forEach(doc => {
        areaID = doc.data().areaID; 
        museumID = doc.data().museumID; 
    });

    //Get area info
    if(areaID!= null){
        const sfRef = db.collection('MuseumArea').doc(areaID);
        const doc = await sfRef.get();
        if (!doc.exists) {
            console.log('No such document! - MuseumArea');
            return;
        } else {
            var tem = Object.assign({}, doc.data());
            tem['_id'] = doc.id;

            result += JSON.stringify(tem);
            
        }
    }

    //get area list
    const AreaRef = db.collection('MuseumArea');
    const snapshot2 = await AreaRef
    .where('museumID', '==', museumID)
    .get();

    if (snapshot2.empty) {
        console.log('No matching documents.');
        return;
    }

    var cnt = 1;
    result += "],\"list\":["
    snapshot2.forEach(doc => {
        var tem = Object.assign({}, doc.data());
        tem['_id'] = doc.id;
        if(cnt == 1){
            result += JSON.stringify(tem)
            cnt++
        }else{
            result += ',' + JSON.stringify(tem)
            cnt++
        }
    });
    result += ']}'

    console.log(result)

    return JSON.parse(result)
}


app.listen(3000, () => console.log('Example app listening on port 3000!'))