const firebase = require('firebase')
const config = require('../Back-end/config')

const db = firebase.initializeApp(config.firebaseConfig);

module.exports = db