class Exhibition{
    constructor( id,museumId,  name_cn,  name_en,  date,  intro_cn,  intro_en,  pic_url,  website_url,  isSpecialExhibition, museumName_cn, museumName_en) {
        this.id = id;
        this.museumId = museumId;
        this.name_cn = name_cn;
        this.name_en = name_en;
        this.date = date;
        this.intro_cn = intro_cn;
        this.intro_en = intro_en;
        this.pic_url = pic_url;
        this.website_url = website_url;
        this.isSpecialExhibition = isSpecialExhibition;
        this.museumName_cn = museumName_cn;
        this.museumName_en = museumName_en;
    }
}

module.exports = Exhibition