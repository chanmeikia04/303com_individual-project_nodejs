class FloorPlan{
    constructor(sX,sY, dX, dY, floorPlan) {
        this.dX = dX;
        this.dY = dY;
        this.sX = sX;
        this.sY = sY;
        this.floorPlan = floorPlan;
    }
}

module.exports = FloorPlan