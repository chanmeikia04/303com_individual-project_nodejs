class AdditionBeacon{
    constructor( id , pic_url,  website_url,  major,  uuid,  minor,  name_cn,  name_en) {
        this.id = id;
        this.pic_url = pic_url;
        this.website_url = website_url;
        this.major = major;
        this.uuid = uuid;
        this.minor = minor;
        this.name_cn = name_cn;
        this.name_en = name_en;
    }
}

module.exports = AdditionBeacon