class MuseumArea{
    constructor( id , museumID, area_cn, area_en, name_cn, name_en, pointX, pointY, floorPlan) {
        this.id = id;
        this.area_cn = area_cn;
        this.area_en = area_en;
        this.museumID = museumID;
        this.name_cn = name_cn;
        this.name_en = name_en;
        this.pointX = pointX;
        this.pointY = pointY;
        this.floorPlan = floorPlan;
    }
}

module.exports = MuseumArea