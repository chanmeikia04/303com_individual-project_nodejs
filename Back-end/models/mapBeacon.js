class MapBeacon{
    constructor( id , major,  uuid,  minor,  areaID,  museumID, area_cn, area_en, museumName_cn, museumName_en) {
        this.id = id;
        this.major = major;
        this.uuid = uuid;
        this.minor = minor;
        this.areaID = areaID;
        this.area_cn = area_cn;
        this.area_en = area_en;
        this.museumID = museumID;
        this.museumName_cn = museumName_cn;
        this.museumName_en = museumName_en;
    }
}

module.exports = MapBeacon