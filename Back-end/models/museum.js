class Museum{
    constructor( id,name_en,  name_cn,  address_en,  address_cn,  openingHours,  telephone,  website,  pic_url,  remark_en,  remark_cn, floorPlan) {
            this.id = id;
            this.name_en = name_en;
            this.name_cn = name_cn;
            this.address_en = address_en;
            this.address_cn = address_cn;
            this.openingHours = openingHours;
            this.telephone = telephone;
            this.website = website;
            this.pic_url = pic_url;
            this.remark_en = remark_en;
            this.remark_cn = remark_cn;
            this.floorPlan = floorPlan;
    }
}

module.exports = Museum