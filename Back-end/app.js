const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const config = require('../Back-end/config')

const museumRoutes = require('./routes/museum-routes')
const exhibitionRoutes = require('./routes/exhibition-routes')
const additionalBeaconRoutes = require('./routes/additionalBeacon-routes')
const mapBeaconRoutes = require('./routes/mapBeacon-routes')
const museumAreaRoutes = require('./routes/museumArea-routes')

const app = express()

app.use(express.json())
app.use(cors())
app.use(bodyParser.json())

app.use('/api', museumRoutes.routers)
app.use('/exhibitionApi', exhibitionRoutes.routers)
app.use('/additionalInfoApi', additionalBeaconRoutes.routers)
app.use('/mapBeaconApi', mapBeaconRoutes.routers)
app.use('/museumAreaApi', museumAreaRoutes.routers)

app.listen(config.port, () => console.log('App is listening on url http://localhost:' + config.port))