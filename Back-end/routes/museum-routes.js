const { request } = require('express')
const express = require('express')
const {addMuseum, getAllMuseums, getMuseum, updateMuseum, deleteMuseum} = require('../controllers/museumControllers')

const router = express.Router()

router.post('/museum', addMuseum)
router.get('/museums', getAllMuseums)
router.get('/museum/:id', getMuseum)
router.patch('/museum/:id', updateMuseum)
router.delete('/museum/:id', deleteMuseum)


module.exports={
    routers: router
}
