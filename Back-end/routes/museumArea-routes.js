const { request } = require('express')
const express = require('express')
const {addArea, getAllArea, getAreaByID,getAreaByTwoID, updateArea, deleteArea, getAreaByMuseumID} = require('../controllers/museumAreaControllers')

const router = express.Router()

router.post('/', addArea)
router.get('/', getAllArea)
router.get('/:id/:id2', getAreaByTwoID)
router.get('/area/museumID/:id', getAreaByMuseumID)
router.get('/:id', getAreaByID)
router.patch('/:id', updateArea)
router.delete('/:id', deleteArea)


module.exports={ routers: router }
