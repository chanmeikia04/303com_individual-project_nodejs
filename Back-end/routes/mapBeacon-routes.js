const { request } = require('express')
const express = require('express')
const {addMapBeacon, getAllMapBeacon, getMapBeaconByBeaconInfo, getMapBeaconBeaconByID, updateMapBeacon, deleteMapBeacon} = require('../controllers/mapBeaconControllers')

const router = express.Router()

router.post('/', addMapBeacon)
router.get('/', getAllMapBeacon)
router.get('/:id', getMapBeaconBeaconByID)
router.get('/:uuid/:minor/:major', getMapBeaconByBeaconInfo)
router.patch('/:id', updateMapBeacon)
router.delete('/:id', deleteMapBeacon)


module.exports={ routers: router }
