const { request } = require('express')
const express = require('express')
const {addExhibition,getAllExhibition,getExhibitionByID, getExhibition,updateExhibition,deleteExhibition} = require('../controllers/exhibitionControllers')

const router = express.Router()

router.post('/exhibition', addExhibition)
router.get('/', getAllExhibition)
router.get('/id/:id', getExhibitionByID)
router.get('/:id', getExhibition)
router.patch('/exhibition/:id', updateExhibition)
router.delete('/exhibition/:id', deleteExhibition)


module.exports={
    routers: router
}