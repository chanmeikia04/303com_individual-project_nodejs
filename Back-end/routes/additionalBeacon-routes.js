const { request } = require('express')
const express = require('express')
const {addAdditionBeacon, getAllAdditionBeacon, getAdditionBeaconByID, getAdditionBeaconByBeaconInfo, updateAdditionBeacon, deleteAdditionBeacon} = require('../controllers/additionalBeaconControllers')

const router = express.Router()

router.post('/', addAdditionBeacon)
router.get('/', getAllAdditionBeacon)
router.get('/:id', getAdditionBeaconByID)
router.get('/:uuid/:minor/:major', getAdditionBeaconByBeaconInfo)
router.patch('/:id', updateAdditionBeacon)
router.delete('/:id', deleteAdditionBeacon)


module.exports={
    routers: router
}
