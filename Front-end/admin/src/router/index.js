import Vue from 'vue'
import Router from 'vue-router'
import Signinpage from '@/components/Signin'
import Signoutpage from '@/components/Signout'
import MuseumListpage from '@/components/MuseumManagement/MuseumList'
import MuseumCreatepage from '@/components/MuseumManagement/CreateMuseum'
import MuseumModifypage from '@/components/MuseumManagement/ModifyMuseum'
import MuseumAreaListpage from '@/components/MuseumArea/MuseumAreaList'
import MuseumAreaListCreatepage from '@/components/MuseumArea/CreateMuseumArea'
import MuseumAreaListModifypage from '@/components/MuseumArea/ModifyMuseumArea'
import MapBeaconListpage from '@/components/Beacon/MapBeaconList'
import MapBeaconCreatepage from '@/components/Beacon/CreateMapBeacon'
import MapBeaconModifypage from '@/components/Beacon/ModifyMapBeacon'
import AdditionalBeaconListpage from '@/components/AdditionalBeacon/AdditionalBeaconList'
import AdditionalBeaconCreatepage from '@/components/AdditionalBeacon/CreateAdditionalBeacon'
import AdditionalBeaconModifypage from '@/components/AdditionalBeacon/ModifyAdditionalBeacon'
import ExhibitionListpage from '@/components/Exhibition/ExhibitionList'
import ExhibitionCreatepage from '@/components/Exhibition/CreateExhibition'
import ExhibitionModifypage from '@/components/Exhibition/ModifyExhibition'


Vue.use(Router)

export default new Router({
    routes: [
      {
        path: '/',
        name: 'SignIn',
        component: Signinpage,
        meta: {requireAuth: false}   
      },
      {
        path: '/SignOut',
        name: 'SignOut',
        component: Signoutpage,
        meta: {requireAuth: true}   
      },
      {
        path: '/museums',
        name: 'MuseumList',
        component: MuseumListpage,
        meta: {requireAuth: true}   
      },
      {
        path: '/museumc',
        name: 'CreateMuseum',
        component: MuseumCreatepage,
        meta: {requireAuth: true}   
      },
      {
        path: '/museumm',
        name: 'ModifyMuseum',
        component: MuseumModifypage,
        meta: {requireAuth: true}   
      },
      {
        path: '/museumAreas',
        name: 'MuseumAreaList',
        component: MuseumAreaListpage,
        meta: {requireAuth: true}   
      },
      {
        path: '/museumAreac',
        name: 'CreateMuseumArea',
        component: MuseumAreaListCreatepage,
        meta: {requireAuth: true}   
      },
      {
        path: '/museumAream',
        name: 'ModifyMuseumArea',
        component: MuseumAreaListModifypage,
        meta: {requireAuth: true}   
      },
      {
        path: '/mapBeacons',
        name: 'MapBeaconList',
        component: MapBeaconListpage,
        meta: {requireAuth: true}   
      },
      {
        path: '/mapBeaconc',
        name: 'CreateMapBeacon',
        component: MapBeaconCreatepage,
        meta: {requireAuth: true}   
      },
      {
        path: '/mapBeaconm',
        name: 'ModifyMapBeacon',
        component: MapBeaconModifypage,
        meta: {requireAuth: true}   
      },
      {
        path: '/additionalBeacons',
        name: 'AdditionalBeaconList',
        component: AdditionalBeaconListpage,
        meta: {requireAuth: true}   
      },
      {
        path: '/additionalBeaconc',
        name: 'CreateAdditionalBeacon',
        component: AdditionalBeaconCreatepage,
        meta: {requireAuth: true}   
      },
      {
        path: '/additionalBeaconm',
        name: 'ModifyAdditionalBeacon',
        component: AdditionalBeaconModifypage,
        meta: {requireAuth: true}   
      },
      {
        path: '/exhibitions',
        name: 'ExhibitionList',
        component: ExhibitionListpage,
        meta: {requireAuth: true}   
      },
      {
        path: '/exhibitionc',
        name: 'CreateExhibition',
        component: ExhibitionCreatepage,
        meta: {requireAuth: true}   
      },
      {
        path: '/exhibitionm',
        name: 'ModifyExhibition',
        component: ExhibitionModifypage,
        meta: {requireAuth: true}   
      }

      
      
    ]
})