//Firebase
import firebase from "firebase/app";
import "firebase/storage";

//vue
import Vue from 'vue'
import App from './App.vue'
import router from './router'

//Bootstrap
import {BootstrapVue, BootstrapVueIcons} from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.component('Navbar',require('./components/tool/NavBar.vue').default);
Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  // console.log('上一个页面：', from)
  // console.log('下一个页面：', to)
 
  let userToken = localStorage.getItem('username')
 
  if (to.meta.requireAuth) {
    if (userToken) {
      next()
    } else {
      next({
        path: '/'
      })
    }
  } else {
    next()
  }
 
  if (to.fullPath === '/') {
    if (userToken) {
      next({
        path: from.fullPath
      })
    } else {
      next()
    }
  }
})

new Vue({
  router,
  render: h => h(App),

  created() {
    this.$router.push('/')

    //Firebase configuration
    var firebaseConfig = {
      apiKey: "AIzaSyCwZA5h43aDzLjzjZypwYaw8K_y4qttOoc",
      authDomain: "com-3592a.firebaseapp.com",
      projectId: "com-3592a",
      storageBucket: "com-3592a.appspot.com",
      messagingSenderId: "960425179514",
      appId: "1:960425179514:web:7de382773d4685501efec3",
      measurementId: "G-DQ26TQ2NLS"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
  }
}).$mount('#app')
